      
      <div class="row">
        <h3 class="text-center">INGRESE AL SISTEMA</h3>
        <form class="form-horizontal col-offset-3" action="<?=base_url()?>welcome/entrar" method="post">
          <div class="form-group">
            <label for="Usuario" class="col-lg-2 control-label">Usuario</label>
            <div class="col-lg-6">
              <input type="text" class="form-control" id="Usuario" placeholder="Usuario" name="usuario">
            <?php if (form_error('usuario')): ?>
            <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?=form_error('usuario')?>
            </div>
            <?php endif ?>
            </div>
          </div>
          <div class="form-group">
            <label for="Clave" class="col-lg-2 control-label">Clave</label>
            <div class="col-lg-6">
              <input type="password" class="form-control" id="Clave" placeholder="Clave" name="clave">
              <?php if (form_error('clave')): ?>
              <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?=form_error('clave')?>
              </div>
              <?php endif ?>
            </div>
          </div>
            <button type="submit" class="btn btn-default col-offset-2 col-lg-2">Entrar</button>
          <div class="form-group">
          </div>
        </form>

        <?php if (!empty($mensaje)): ?>
        <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
          <h5>Se han encontrado errores</h5>
            <?php echo $mensaje['mensaje']; ?>
        </div>
        <?php endif ?>
          
      </div>
      