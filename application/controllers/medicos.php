<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medicos extends CI_Controller {
  
  private $sources;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in")  || !$this->session->userdata("tipo")=="Administrador"){
      redirect('/');
    }
    
    $this->sources = $this->constantes->assets();

    $this->load->library("grocery_CRUD");
   
  }
  
  public function mostrar($output = null, $vista = "lmedicos_vista")
  {
  
    $this->sources['css_files'] = $output->css_files;
    $this->load->view('cabeceraadmin_vista', $this->sources);
    $this->load->view($vista, $output);
    $this->sources['js_files'] = $output->js_files;
    $this->load->view('pieadmin_vista', $this->sources);

  }

  public function lmedicos()
  {
    
    $medicos = new grocery_CRUD();
    $medicos->set_table("medicos");
    $medicos->set_subject("medico");
    $medicos->set_relation('especialidad_id','especialidades','nombre');
    $medicos->set_theme("datatables");
    $medicos->display_as('cedula', 'Cédula');
    $medicos->display_as('matricula', 'Matrícula');
    $medicos->display_as('nombres', 'Nombres y Apellidos');
    $medicos->display_as('especialidad_id', 'Especialidad');
    $medicos->set_rules('cedula', 'Cédula',"required|numeric|min_length[6]|max_length[12]");
    $medicos->set_rules('nombres', 'Nombres y Apellidos',"required|alpha_space|min_length[2]|max_length[100]");
    $medicos->set_rules('matricula', 'Matrícula',"alpha_numeric|min_length[4]|max_length[10]");
    $medicos->set_rules('especialidad_id', 'Especialidad',"required");
    $output = $medicos->render();
    $this->mostrar($output);
  
  }

  public function lespecialidades()
  {
    
    $especialidades = new grocery_CRUD();
    $especialidades->set_table("especialidades");
    $especialidades->set_subject("especialidad");
    $especialidades->set_theme("datatables");
    $especialidades->display_as('nombre', 'Especialidad');
    $especialidades->set_rules('nombre', 'Especialidad',"required|alpha_space|min_length[2]|max_length[75]");
    $output = $especialidades->render();
    $this->mostrar($output);
  
  }

  public function nmedico()
  {
    
    redirect("medicos/lmedicos/add");
  
  }

  public function nespecialidad()
  {
    
    redirect("medicos/lespecialidades/add");
  
  }

  public function atras()
  {
    
    redirect("admin");
  
  }

}
